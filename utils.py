import json
import csv
from datetime import datetime
import pathlib
import os
from emailsender import EmailSender

class Utils:
    @staticmethod
    def get_json_data(filename):
        with open(filename) as json_file:
            data = json.load(json_file)
        return data["Printers"]

    @staticmethod
    def export_to_csv(printers_list):
        if printers_list and len(printers_list) > 0:
            path = pathlib.Path(__file__).parent.absolute()
            now = datetime.now()
            # for full path add this at the beginning:  str(path) + os.path.sep +
            filename_with_path = "csv" + os.path.sep + now.strftime("%d-%m-%Y_%H-%M-%S" + '.csv')
            keys = printers_list[0].keys()
            with open(filename_with_path, 'w', newline='') as output_file:
                dict_writer = csv.DictWriter(output_file, keys)
                dict_writer.writeheader()
                dict_writer.writerows(printers_list)
            return filename_with_path
        else:
            return ""

    @staticmethod
    def last_month_name():
        now = datetime.now()
        last_month = now.month - 1 if now.month > 1 else 12

        return "Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec".split()[last_month - 1]

    @staticmethod
    def send_email(filename=""):
        subject = "Printers Counter - Month of " + Utils.last_month_name()
        body = ""
        if EmailSender.send_mail("printercounter@email.sender.com", ['recp1@test.com', 'recpt2@test.com'], subject,
                              body, filename, "192.168.3.11", 25):
            print('Email Sent')
        else:
            print('Error Sending Email')
