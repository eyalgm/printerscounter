from utils import Utils
import time
from printers import Printers
from datetime import datetime

# first installation :
# apt-get install python3-pip
# pip3 install snmpclitools-lextudio
# pip3 install python-dateutil

# its not working with python 3.9+
# need to install python3.8 & after run this cmd: sudo apt install python3.8-distutils
# pyasn1 version 0.4.8 is working and not version 0.5.0



start_time = time.time()
printers_list = Utils.get_json_data('printers.json')
if printers_list:
    Printers.import_from_json(printers_list)
    print("Please wait...")
    Printers.set_printers_data(1)
    filename = Printers.export_to_csv()
    #if filename:
        #print("Sending Email...")
        #time.sleep(1)  #Sleep for 1 seconds
        #Utils.send_email(filename)


end_time = time.time()

def_time = round(end_time - start_time)

print(f"\nTotal time: {def_time} Seconds.")

#print(def_time)

# test :
'''
MAX_INT_NUM = 27
threadsNumber = 3
min_num = 1
max_num = int(MAX_INT_NUM / threadsNumber)
if max_num > 0:
    index = 2
    while index <= threadsNumber+1:
        print(f"{min_num} - {max_num}")
        min_num = max_num + 1
        max_num = int((MAX_INT_NUM / threadsNumber)) * index if index <= threadsNumber-1 else MAX_INT_NUM
        index += 1
'''

#Utils.send_email("csv/26-01-2022_11-03-45.csv")
