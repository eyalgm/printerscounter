import threading
import printerdata


class PrinterThread (threading.Thread):
    def __init__(self, thread_id, printers_list, minimum, maximum, print_to_console):
        threading.Thread.__init__(self)
        self.__id = thread_id
        self.__printers_list = printers_list
        self.__min = minimum
        self.__max = maximum
        self.__print_to_console = print_to_console
        self.__return = None

    def run(self):
        thread_list = []
        if self.__min < self.__max <= len(self.__printers_list):
            index = self.__min
            while index <= self.__max:
                if self.__print_to_console == 1: print(f"Thread {self.__id}")
                thread_list.append(printerdata.PrinterData.get_printer_data(self.__printers_list[index-1], self.__print_to_console))
                index += 1
        self.__return = thread_list

    def join(self):
        threading.Thread.join(self)
        return self.__return

    @property
    def get_name(self):
        return self.__name

    @property
    def get_result(self):
        return self.__return
