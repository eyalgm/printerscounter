from pysnmp import hlapi
import quicksnmp
from network import Network
import printers


class PrinterData:
    # Constants
    __COMMUNITY = "public"
    __OID_SN = "1.3.6.1.2.1.43.5.1.1.17.1"  # Serial Number
    __RICOH_OID_SN = "1.3.6.1.4.1.367.3.2.1.2.1.4.0"  # Richo Serial Number
    __OID_MN = "1.3.6.1.2.1.43.5.1.1.16.1"  # "1.3.6.1.2.1.25.3.2.1.3.1"  Model Name
    __OID_PC = "1.3.6.1.2.1.43.10.2.1.4.1.1"  # Page Counter

    @classmethod
    def get_printer_data(cls, printer, print_to_console=0):
        if printer and isinstance(printer, printers.Printers.Printer):
            if Network.ping_ok(printer.ip):
                var_temp = ""
                try:
                    var_temp = (quicksnmp.get(printer.ip, [cls.__OID_PC],
                                                     hlapi.CommunityData(cls.__COMMUNITY)))
                    printer.totalpages = int(next(iter(var_temp.values())))
                except (Exception,):
                    printer.totalpages = -1
                var_temp = ""
                try:
                    var_temp = quicksnmp.get(printer.ip, [cls.__OID_MN],
                                                              hlapi.CommunityData(cls.__COMMUNITY))
                    printer.model = next(iter(var_temp.values()))
                except (Exception,):
                    printer.model = ""
                var_temp = ""
                try:
                    oid_serial_number = cls.__OID_SN
                    if printer.manufacturer == printers.Printers.Manufacturer.RICOH:
                        oid_serial_number = cls.__RICOH_OID_SN
                    var_temp = quicksnmp.get(printer.ip, [oid_serial_number], hlapi.CommunityData(cls.__COMMUNITY))
                    printer.serialnumber = next(iter(var_temp.values()))
                except (Exception,):
                    printer.serialnumber = ""
                if print_to_console == 1:
                    print(f"{printer.ip} : {str(printer.totalpages)} Total Pages.")
                    print(f"{printer.ip} : {printer.model} Model Name.")
                    print(f"{printer.ip} : {printer.serialnumber} Serial Number")
            else:
                if print_to_console == 1:
                    print(f"{printer.ip} : No Ping.")
        return printer
