import os
import smtplib
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# https://mailtrap.io/blog/sending-emails-in-python-tutorial-with-code-examples/


class EmailSender:
    @staticmethod
    def send_mail(send_from, send_to, subject, body, filename_with_path="",
                  server="localhost", port=25, username='', password=''):

        sender_email = send_from
        receiver_email = ", ".join(send_to)

        message = MIMEMultipart()
        message["From"] = sender_email
        message["To"] = receiver_email
        message["Subject"] = subject

        # Add body to email
        message.attach(MIMEText(body, "plain"))

        if filename_with_path and os.path.isfile(filename_with_path):
            filename_with_path_list = filename_with_path.split(os.path.sep)
            if len(filename_with_path) > 1:
                filename = filename_with_path_list[-1]
            else:
                filename = filename_with_path
            # We assume that the file is in the directory where you run your Python script from
            with open(filename_with_path, "rb") as attachment:
                # The content type "application/octet-stream" means that a MIME attachment is a binary file
                part = MIMEBase("application", "octet-stream")
                part.set_payload(attachment.read())

            # Encode to base64
            encoders.encode_base64(part)

            # Add header
            part.add_header(
                "Content-Disposition",
                f"attachment; filename= {filename}",
            )
            # Add attachment to your message and convert it to string
            message.attach(part)

        text = message.as_string()

        try:
            # send your email
            with smtplib.SMTP(server, port) as smtp_server:
                if username and password:
                    smtp_server.login(username, password)
                smtp_server.sendmail(sender_email, send_to, text)
                smtp_server.quit()
        except (Exception,):
            return 0
        return 1
