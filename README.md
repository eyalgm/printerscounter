Printers Counter Python App

Before running the App you need to create :
1. csv directory
2. printers.json file
3. pip3 install -r requirements.txt

json printers example:

{
    "Printers": [
      {
        "PrinterManufacturer": "HP",
        "PrinterIP": "10.0.0.10"
      },
      {
        "PrinterManufacturer": "RICOH",
        "PrinterIP": "10.0.0.12"
      }
    ]
}

Printer Manufacturer : HP \ RICOH \ TOSHIBA

For running the app use:

python3 main.py