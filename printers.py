from enum import Enum
import printerthread
from utils import Utils


class Printers:
    __list = []
    __PRINTER_MANUFACTURER = "PrinterManufacturer"
    __PRINTER_IP = "PrinterIP"
    __MODEL_NAME = "ModelName"
    __TOTAL_PAGES = "TotalPages"
    __SERIAL_NUMBER = "SerialNumber"

    @classmethod
    def get_list(cls):
        return cls.__list

    @classmethod
    def append_list(cls, printer):
        if not isinstance(printer, Printers.Printer):
            raise ValueError("Invalid printer")
        cls.__list.append(printer)

    @classmethod
    def import_from_json(cls, json: str):
        for printer in json:
            cls.Printer(Printers.Manufacturer.string_to_manufacturer(printer["PrinterManufacturer"]),
                        printer["PrinterIP"])

    @classmethod
    def import_from_xml(cls, xml: str):
        pass

    @classmethod
    def export_to_csv(cls):
        if len(cls.__list) > 0:
            return Utils.export_to_csv(cls.to_list())

    @classmethod
    def to_list(cls):
        p_list = []
        if cls.__list:
            for printer in cls.__list:
                p_dict = {
                    cls.__PRINTER_IP: printer.ip,
                    cls.__PRINTER_MANUFACTURER: printer.manufacturer.name,
                    cls.__SERIAL_NUMBER: printer.serialnumber, cls.__MODEL_NAME: printer.model,
                    cls.__TOTAL_PAGES: str(printer.totalpages) if printer.totalpages > 0 else str("")
                }
                p_list.append(p_dict)
        return p_list

    @classmethod
    def set_printers_data(cls, print_to_console=0):
        if cls.__list:
            '''for index, printer in enumerate(cls.__list):
                if print_to_console == 1:
                    print(printer.ip + " Checking ...", end='\r')
                cls.__list[index] = printerdata.PrinterData.get_printer_data(printer, print_to_console)'''
            MAX_INT_NUM = len(cls.__list)
            threadsNumber = 3 if MAX_INT_NUM > 2 else 1
            threads_list = []
            min_num = 1
            max_num = int(MAX_INT_NUM / threadsNumber)
            if max_num > 0:
                index = 2
                while index <= threadsNumber + 1:
                    #print(f"{min_num} - {max_num}")
                    thread = printerthread.PrinterThread(index-1, cls.__list, min_num, max_num, print_to_console)
                    threads_list.append(thread)
                    min_num = max_num + 1
                    max_num = int((MAX_INT_NUM / threadsNumber)) * index if index <= threadsNumber - 1 else MAX_INT_NUM
                    index += 1
                for t in threads_list:
                    t.start()
                for t in threads_list:
                    t.join()
                new_printers_list = []
                for t in threads_list:
                    new_printers_list = new_printers_list + t.get_result

                cls.__list = []
                cls.__list = list(new_printers_list)

    class Manufacturer(Enum):
        HP = 1
        RICOH = 2
        TOSHIBA = 3

        @classmethod
        def string_to_manufacturer(cls,name:str):
            if name == 'HP':
                return cls.HP
            elif name == "RICOH":
                return cls.RICOH
            elif name == "TOSHIBA":
                return cls.TOSHIBA
            else:
                raise ValueError("invalid manufacturer name.")

    class Printer:
        def __init__(self, manufacturer, ip: str, serialnumber="", model="", totalpages=-1):
            if isinstance(manufacturer, Printers.Manufacturer):
                self.__manufacturer = manufacturer
            else:
                raise ValueError("Invalid manufacturer")
            self.__ip = ip
            self.__serialnumber = serialnumber
            self.__model = model
            self.__totalpages = totalpages
            Printers.append_list(self)

        @property
        def manufacturer(self):
            return self.__manufacturer

        @property
        def ip(self):
            return self.__ip

        @property
        def serialnumber(self):
            return self.__serialnumber

        @serialnumber.setter
        def serialnumber(self, value):
            self.__serialnumber=value

        @property
        def model(self):
            return self.__model

        @model.setter
        def model(self, value):
            self.__model=value

        @property
        def totalpages(self):
            return self.__totalpages

        @totalpages.setter
        def totalpages(self, value: int):
            self.__totalpages=value

        def __repr__(self):
            return f"Printer({self.__manufacturer.name},{self.__ip},{self.__serialnumber},{self.__model},{self.__totalpages})"

