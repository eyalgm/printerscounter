import os
import subprocess, platform


# Network Moudle

class Network:

    @staticmethod
    def check_ping(ip):
        str_ping = "ping -"
        if platform.system().lower() == "windows":
            str_ping += "n"
        else:
            str_ping += "c"
        str_ping += " 3 " + ip
        response = os.system(str_ping)
        # and then check the response...
        if response == 0:
            ping_status = True
        else:
            ping_status = False

        return ping_status

    @staticmethod
    def ping_ok(host):
        try:
            output = subprocess.check_output(
                "ping -{} 3 -{} 2 {}".format('n' if platform.system().lower() == "windows" else 'c',
                                             'w' if platform.system().lower() == "windows" else 'W', host), shell=True)
        except (Exception,):
            return False
        return True
